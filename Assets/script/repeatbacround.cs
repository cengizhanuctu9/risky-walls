using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class repeatbacround : MonoBehaviour
{
    MeshRenderer mesren;
    private void Start()
    {
        mesren = GetComponent<MeshRenderer>();
    }
    void Update()
    {
        float x = Mathf.Repeat(Time.time * 0.1f, 1);
        Vector2 offset = new Vector2(x, 0);
        mesren.sharedMaterial.SetTextureOffset("_MainTex", offset);
    }
}
