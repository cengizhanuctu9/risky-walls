using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class engel : MonoBehaviour
{
    public float speed;

    private void Start()
    {
        float ye = Random.Range(1, 6);
        transform.localScale= new Vector3(ye, ye, 1);
    }

    private void FixedUpdate()
    {
        transform.position += Vector3.left * speed * Time.deltaTime;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("duvar"))
        {
            gameObject.SetActive(false);
        }
       
    }
    
}
