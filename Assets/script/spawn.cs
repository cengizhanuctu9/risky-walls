using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn : MonoBehaviour
{
    public GameObject engel;
    public GameObject para;
    public GameObject magnet;
    GameObject[] poolengel;
    GameObject[] poolpara;
    GameObject[] poolmiknatis;
    int engelcount=10;
    int paracount=5;
    int magnetcount=2;
    int time;
    int indexengel;
    int indexpara;
    int indexmagnet;
    float rangey;
    void Start()
    {
       
        StartCoroutine(spawnobj(0));// ald��� paramaetre hangi fonsyon �ag�r�lacag�
        StartCoroutine(spawnobj(1));
        StartCoroutine(spawnobj(2));
        objepoolengel(engel, engelcount);//ald��� paramaetreler hangi obje �retilecegi ve ka� tane �retilecegi 
        objepoolpara(para, paracount);
        objepoolmiknatis(magnet, magnetcount);
    }

    
  
    IEnumerator spawnobj(int fonkscount)
    {
        while (true)
        {
            if (fonkscount == 0)
            {
                spawnetengel();
                time = 3;
            }
            if (fonkscount == 1)
            {
                spawnetpara();
                time = 1;
            }
            if (fonkscount == 2)
            {
                time = 10;
                spawnetmiknatis();
            }

            yield return new WaitForSeconds(time);
        }
      
    }
    void objepoolengel(GameObject obj , int countpool)
    {
        poolengel = new GameObject[countpool];
        for(int i =0; i < countpool; i++)
        {
            GameObject ob= Instantiate(obj);
            poolengel[i] = ob;
            ob.SetActive(false);
        }
    }
    void objepoolpara(GameObject obj, int countpool)
    {
        poolpara = new GameObject[countpool];
        for (int i = 0; i < countpool; i++)
        {
            GameObject ob = Instantiate(obj);
            poolpara[i] = ob;
            ob.SetActive(false);
        }
    }
    void objepoolmiknatis(GameObject obj, int countpool)
    {
        poolmiknatis = new GameObject[countpool];
        for (int i = 0; i < countpool; i++)
        {
            GameObject ob = Instantiate(obj);
            poolmiknatis[i] = ob;
            ob.SetActive(false);
        }
    }
    void spawnetengel()
    {
       
       
        if (poolengel != null)
        {
            rangey = Random.Range(-3.2f, 3.1f);
            poolengel[indexengel].transform.position=new Vector3(transform.position.x,rangey,0);
            poolengel[indexengel].SetActive(true);
            indexengel++;
        }
        if (indexengel >=engelcount)
        {
            indexengel = 0;
        }

    }
    void spawnetpara()
    {

       
        if (poolpara != null)
        {
           float rangep = Random.Range(-3.2f, 3.1f);
            poolpara[indexpara].transform.position = new Vector3(transform.position.x, rangey, 0);
            poolpara[indexpara].SetActive(true);
            indexpara++;
        }
        if (indexpara >= paracount)
        {
            indexpara = 0;
        }

    }
    void spawnetmiknatis()
    {

        
        if (poolmiknatis != null)
        {
           float rangeym = Random.Range(-3.2f, 3.1f);
            poolmiknatis[indexmagnet].transform.position = new Vector3(transform.position.x, rangey, 0);
            poolmiknatis[indexmagnet].SetActive(true);
            indexmagnet++;
        }
        if (indexmagnet >= magnetcount)
        {
            indexmagnet = 0;
        }

    }

}
