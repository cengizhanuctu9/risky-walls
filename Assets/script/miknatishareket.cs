using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class miknatishareket : MonoBehaviour
{
    public float speed;
    private void FixedUpdate()
    {
        transform.position += Vector3.left * speed * Time.deltaTime;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("engel"))
        {
            gameObject.SetActive(false);
        }
        else if (collision.CompareTag("duvar"))
        {
            gameObject.SetActive(false);
        }
    }
    }
