using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class movesc : MonoBehaviour
{
    Rigidbody2D rg;
    float hiz =10;
    public Text puantxt;
    int puan;
    AudioSource coinses;
    public AudioClip[] sesler;
    public static bool ismagnet;
    float zaman;
    public GameObject patlamaanim;
    public GameObject panel;
    private bool dontmousecilik=false;

    void Start()
    {
        ismagnet = false;
        coinses = GetComponent<AudioSource>();
        rg = GetComponent<Rigidbody2D>();  
    }


    private void Update()
    {
       
            move();
        
      
    }
    private void move()
    {
       
       rg.velocity = new Vector2(0, hiz);

        if (Input.GetMouseButtonDown(0)&&!dontmousecilik)
        {
            hiz *= -1;
            coinses.PlayOneShot(sesler[1], 0.7f);
           

        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.CompareTag("sinir"))
        {
            hiz *= -1;
        }

    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("engel"))
        {
            Instantiate(patlamaanim, transform.position, Quaternion.identity);
            var render = gameObject.GetComponent<SpriteRenderer>();
            render.enabled = false;
            coinses.Stop();
            StartCoroutine(stoptime());


        }

        if (collision.gameObject.CompareTag("para"))
        {
            coinses.PlayOneShot(sesler[0],0.7f);
            puan++;
            puantxt.text = puan.ToString();
        }
        if (collision.gameObject.CompareTag("miknatis"))
        {
            coinses.PlayOneShot(sesler[2], 0.7f);
            StartCoroutine(stopmagnet());
            collision.gameObject.SetActive(false);
            
        }
        if (collision.gameObject.CompareTag("dontmousecilik"))
        {

            dontmousecilik = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)// hatayi ��zmek i�in yapt�m �arp��ma an�nda tiklanma oldugunda y�zete yap���yordu
    {
        if (collision.gameObject.CompareTag("dontmousecilik"))
        {

            dontmousecilik = false;
        }
    }
    IEnumerator stopmagnet()
    {
        ismagnet = true;
        yield return new WaitForSeconds(5);
        ismagnet = false;
    }
    IEnumerator stoptime()
    {
        
        yield return new WaitForSeconds(0.4f);
        panel.SetActive(true);
     
        Time.timeScale = 0;
    }

  
}
